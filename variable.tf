variable "name" {
    type   = string
    default = "awstrainingwithjaganinstance"
}
variable "region" {
    type    = string
    default = "us-east-1"
}
